module.exports = {
    removeNode: removeNode,
    getAnchorTarget: getAnchorTarget,
    forEach: forEach,
    elementExists: elementExists,
    randomInRange: randomInRange,
    addClass: addClass,
    removeClass: removeClass,
    hasClass: hasClass,
    toggleClass: toggleClass,
    addQueryParam: addQueryParam,
    addEventListener: addEventListener,
    getUrl: getUrl,
    documentReady: documentReady,
    cloneCanvas: cloneCanvas,
    drawImageProp: drawImageProp,
    requestInterval: requestInterval,
    requestTimeout: requestTimeout,
    isSafari: isSafari,
    isInternetExplorer: isInternetExplorer,
    isTouchDevice: isTouchDevice,
    isHidden: isHidden,
    jsonp: jsonp,
    serialize: serialize,
    fitie: fitie,
    closest: closest
}

function removeNode(node) {
    if (node.remove) {
        node.remove();
    } else {
        node.parentNode.removeChild(node);
    }
}

function getAnchorTarget(link) {
    var id = link.hash.replace("#", "");
    return document.getElementById(id) || null;
}

function forEach(nodeList, func) {
    if (nodeList.forEach) {
        nodeList.forEach(func);
    } else {
        Array.prototype.forEach.call(nodeList, func);
    }
}

function elementExists(query) {
    var el = document.querySelector(query);
    return el != null;
}

function randomInRange(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function addClass(element, className) {
    if (element.classList) {
        element.classList.add(className);
    } else {
        element.className += ' ' + className;
    }
}

function removeClass(element, className) {
    if (element.classList) {
        element.classList.remove(className);
    } else {
        var classes = element.className.split(' ');
        var existingIndex = classes.indexOf(className);

        if (existingIndex >= 0) {
            classes.splice(existingIndex, 1);
        }

        element.className = classes.join(' ');
    }
}

function hasClass(element, className) {
    if (element.classList) {
        return element.classList.contains(className);
    } else {
        return element.className.indexOf(className) !== -1;
    }
}

function toggleClass(element, className) {
    if (element.classList) {
        element.classList.toggle(className);
    } else {
        var classes = element.className.split(' ');
        var existingIndex = classes.indexOf(className);

        if (existingIndex >= 0) {
            classes.splice(existingIndex, 1);
        } else {
            classes.push(className);
        }

        element.className = classes.join(' ');
    }
}

function addQueryParam(url, param, value) {
    param = encodeURIComponent(param);
    var a = document.createElement('a')
    a.href = url;
    var regex = new RegExp("(\\?|&amp;|&)(" + param + ")(=([^&]*))*");
    var str = param + (value ? "=" + encodeURIComponent(value) : "");
    var q = a.search.replace(regex, "$1" + str);
    if (q === a.search) {
        a.search += (a.search ? "&" : "") + str;
    }
    return a.href;
}

function addEventListener(el, eventName, handler) {
    if (el != null) {
        if (el.addEventListener) {
            el.addEventListener(eventName, handler);
        } else {
            el.attachEvent('on' + eventName, function() {
                handler.call(el);
            });
        }
    }
}

function getUrl(relativePath) {
    if (relativePath) {
        if (relativePath.charAt(0) === '/') {
            relativePath = relativePath.slice(1);
        }
        return window.location.origin + window.location.pathname + relativePath;
    } else {
        return window.location.origin + window.location.pathname;
    }
}

function documentReady(fn) {
    if (document.readyState != 'loading') {
        fn();
    } else if (document.addEventListener) {
        document.addEventListener('DOMContentLoaded', fn);
    } else {
        document.attachEvent('onreadystatechange', function() {
            if (document.readyState != 'loading')
                fn();
        });
    }
}

function cloneCanvas(sourceCanvas) {
    var destinationCanvas = document.createElement('canvas');
    destinationCanvas.width = sourceCanvas.width;
    destinationCanvas.height = sourceCanvas.height;

    var destCtx = destinationCanvas.getContext('2d');

    destCtx.drawImage(sourceCanvas, 0, 0);

    return destinationCanvas;
}

/**
 * By Ken Fyrstenberg Nilsen
 *
 * drawImageProp(context, image [, x, y, width, height [,offsetX, offsetY]])
 *
 * If image and context are only arguments rectangle will equal canvas
 */
function drawImageProp(ctx, img, x, y, w, h, offsetX, offsetY) {

    if (arguments.length === 2) {
        x = y = 0;
        w = ctx.canvas.width;
        h = ctx.canvas.height;
    }

    // default offset is center
    offsetX = typeof offsetX === "number" ? offsetX : 0.5;
    offsetY = typeof offsetY === "number" ? offsetY : 0.5;

    // keep bounds [0.0, 1.0]
    if (offsetX < 0) offsetX = 0;
    if (offsetY < 0) offsetY = 0;
    if (offsetX > 1) offsetX = 1;
    if (offsetY > 1) offsetY = 1;

    var iw = img.width,
        ih = img.height,
        r = Math.min(w / iw, h / ih),
        nw = iw * r, // new prop. width
        nh = ih * r, // new prop. height
        cx, cy, cw, ch, ar = 1;

    // decide which gap to fill
    if (nw < w) ar = w / nw;
    if (Math.abs(ar - 1) < 1e-14 && nh < h) ar = h / nh; // updated
    nw *= ar;
    nh *= ar;

    // calc source rectangle
    cw = iw / (nw / w);
    ch = ih / (nh / h);

    cx = (iw - cw) * offsetX;
    cy = (ih - ch) * offsetY;

    // make sure source rectangle is valid
    if (cx < 0) cx = 0;
    if (cy < 0) cy = 0;
    if (cw > iw) cw = iw;
    if (ch > ih) ch = ih;

    // fill image in dest. rectangle
    ctx.drawImage(img, cx, cy, cw, ch, x, y, w, h);
}


function requestInterval(fn, delay) {
    if (!window.requestAnimationFrame &&
        !window.webkitRequestAnimationFrame &&
        !(window.mozRequestAnimationFrame && window.mozCancelRequestAnimationFrame) && // Firefox 5 ships without cancel support
        !window.oRequestAnimationFrame &&
        !window.msRequestAnimationFrame)
        return window.setInterval(fn, delay);

    var start = new Date().getTime(),
        handle = new Object();

    function loop() {
        var current = new Date().getTime(),
            delta = current - start;

        if (delta >= delay) {
            fn.call();
            start = new Date().getTime();
        }

        handle.value = requestAnimationFrame(loop);
    };

    handle.value = requestAnimationFrame(loop);
    return handle;
}

function requestTimeout(fn, delay) {
    if (!window.requestAnimationFrame &&
        !window.webkitRequestAnimationFrame &&
        !(window.mozRequestAnimationFrame && window.mozCancelRequestAnimationFrame) && // Firefox 5 ships without cancel support
        !window.oRequestAnimationFrame &&
        !window.msRequestAnimationFrame)
        return window.setTimeout(fn, delay);

    var start = new Date().getTime(),
        handle = new Object();

    function loop() {
        var current = new Date().getTime(),
            delta = current - start;

        delta >= delay ? fn.call() : handle.value = requestAnimationFrame(loop);
    };

    handle.value = requestAnimationFrame(loop);
    return handle;
}

function isSafari() {
    return navigator.vendor && navigator.vendor.indexOf('Apple') > -1 && navigator.userAgent && !navigator.userAgent.match('CriOS');
}

function isInternetExplorer() {
    return /MSIE|Trident/.test(navigator.userAgent);
}

function isTouchDevice() {
  return 'ontouchstart' in window        // works on most browsers
      || navigator.maxTouchPoints;       // works on IE10/11 and Surface
};

function isHidden(el) {
    // Note: does not work on position: fixed elements! - use getComputedStyle().visibility (which is more expensive)
    return (el.offsetParent === null);
}

function jsonp(src, options) {
    var options = options || {},
        callback_name = options.callbackName || 'callback',
        on_success = options.onSuccess || function() {},
        on_timeout = options.onTimeout || function() {},
        timeout = options.timeout || 10;

    var timeout_trigger = window.setTimeout(function() {
        window[callback_name] = function() {};
        on_timeout();
    }, timeout * 1000);

    window[callback_name] = function(data) {
        window.clearTimeout(timeout_trigger);
        on_success(data);
    };

    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.async = true;
    script.src = src;

    document.getElementsByTagName('head')[0].appendChild(script);
}

function serialize(form) {
    var field, l, s = [];
    if (typeof form == 'object' && form.nodeName == "FORM") {
        var len = form.elements.length;
        for (var i = 0; i < len; i++) {
            field = form.elements[i];
            if (field.name && !field.disabled && field.type != 'file' && field.type != 'reset' && field.type != 'submit' && field.type != 'button') {
                if (field.type == 'select-multiple') {
                    l = form.elements[i].options.length;
                    for (var j = 0; j < l; j++) {
                        if (field.options[j].selected)
                            s[s.length] = encodeURIComponent(field.name) + "=" + encodeURIComponent(field.options[j].value);
                    }
                } else if ((field.type != 'checkbox' && field.type != 'radio') || field.checked) {
                    s[s.length] = encodeURIComponent(field.name) + "=" + encodeURIComponent(field.value);
                }
            }
        }
    }
    return s.join('&').replace(/%20/g, '+');
}

function fitie(node) {
    // restrict to valid object-fit value
    var objectFit = node.currentStyle['object-fit'];

    if (!objectFit || !/^(contain|cover|fill)$/.test(objectFit)) return;

    // prepare container styles
    var outerWidth = node.clientWidth;
    var outerHeight = node.clientHeight;
    var outerRatio = outerWidth / outerHeight;

    var name = node.nodeName.toLowerCase();

    var setCSS = node.runtimeStyle;
    var getCSS = node.currentStyle;

    var addEventListener = node.addEventListener || node.attachEvent;
    var removeEventListener = node.removeEventListener || node.detachEvent;
    var on = node.addEventListener ? '' : 'on';
    var img = name === 'img';
    var type = img ? 'load' : 'loadedmetadata';

    addEventListener.call(node, on + type, onload);

    if (node.complete) onload();

    function onload() {
        removeEventListener.call(node, on + type, onload);

        // prepare container styles
        var imgCSS = {
            boxSizing: 'content-box',
            display: 'inline-block',
            overflow: 'hidden'
        };

        'backgroundColor backgroundImage borderColor borderStyle borderWidth bottom fontSize lineHeight height left opacity margin position right top visibility width'.replace(/\w+/g, function(key) {
            imgCSS[key] = getCSS[key];
        });

        // prepare image styles
        setCSS.border = setCSS.margin = setCSS.padding = 0;
        setCSS.display = 'block';
        setCSS.height = setCSS.width = 'auto';
        setCSS.opacity = 1;

        var innerWidth = node.videoWidth || node.width;
        var innerHeight = node.videoHeight || node.height;
        var innerRatio = innerWidth / innerHeight;

        // style container
        var imgx = document.createElement('object-fit');

        imgx.appendChild(node.parentNode.replaceChild(imgx, node));

        for (var key in imgCSS) imgx.runtimeStyle[key] = imgCSS[key];

        // style image
        var newSize;

        if (objectFit === 'fill') {
            if (img) {
                setCSS.width = outerWidth;
                setCSS.height = outerHeight;
            } else {
                setCSS['-ms-transform-origin'] = '0% 0%';
                setCSS['-ms-transform'] = 'scale(' + outerWidth / innerWidth + ',' + outerHeight / innerHeight + ')';
            }
        } else if (innerRatio < outerRatio ? objectFit === 'contain' : objectFit === 'cover') {
            newSize = outerHeight * innerRatio;
            setCSS.width = Math.round(newSize) + 'px';
            setCSS.height = outerHeight + 'px';
            setCSS.marginLeft = Math.round((outerWidth - newSize) / 2) + 'px';
        } else {
            newSize = outerWidth / innerRatio;

            setCSS.width = outerWidth + 'px';
            setCSS.height = Math.round(newSize) + 'px';
            setCSS.marginTop = Math.round((outerHeight - newSize) / 2) + 'px';
        }
    }
}

function closest(arr, closestTo) {
    arr = arr.map(function(x) {
        return parseInt(x, 10);
    });

    var closest = Math.max.apply(null, arr);

    for (var i = 0; i < arr.length; i++) {
        if (arr[i] >= closestTo && arr[i] < closest) closest = arr[i];
    }

    return closest;
}
